//
//  MovieTicketsTests.swift
//  MovieTicketsTests
//
//  Created by Dwayne Langley on 5/1/18.
//  Copyright © 2018 Dwayne Langley. All rights reserved.
//

import XCTest
@testable import MovieTickets

class MovieTicketsTests: XCTestCase {
    
    var purchase: MovieTicketPurchase!
    
    /*
     21 x 9 year-olds, 2D, 90 minute duration, Monday, normal seating == $115.50
     21 x 35 year-old, 2D, 90 minute duration, Thursday, normal seating == $126.00
     10 x 14 year-old students + 11 x 9 year-olds, 2D, 90 minute duration, Monday, normal seating == $120.50
     7 x each type, 3D, 240 minute duration, Thursday, balcony seating == $346.50
     */
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func buy(_ amount: Int, for age: Int, _ studentDiscountApplies: Bool) {
        for _ in 1...amount {
            purchase.addTicket(for: age, isStudent: studentDiscountApplies)
        }
    }
    
    func testNoTickets() {
        purchase = MovieTicketPurchase()
        
        XCTAssertEqual(purchase.finish(), "0.00")
    }
    
    // 4 x 35 year-olds, 2D, 90 minute duration, Tuesday, normal seating == $44.00
    func test2() {
        purchase = MovieTicketPurchase(duration: 90, .tuesday)
        buy(4, for: 35, false)
        XCTAssertEqual(purchase.finish(), "44.00")
    }

    // 4 x 35 year-olds, 3D, 90 minute duration, Tuesday, normal seating == $56.00
    func test3() {
        purchase = MovieTicketPurchase(duration: 90, .tuesday, is3D: true)
        buy(4, for: 35, false)
        XCTAssertEqual(purchase.finish(), "56.00")
    }

    // 21 x 35 year-old, 2D, 90 minute duration, Tuesday, normal seating == $126.00
    func test4() {
        purchase = MovieTicketPurchase(duration: 90, .tuesday)
        buy(21, for: 35, false)
        XCTAssertEqual(purchase.finish(), "126.00")
    }

    // 4 x 35 year-olds, 3D, 90 minute duration, Tuesday, balcony seating == $64.00
    func test5() {
        purchase = MovieTicketPurchase(duration: 90, .tuesday, inBalcony: true, is3D: true)
        buy(4, for: 35, false)
        XCTAssertEqual(purchase.finish(), "64.00")
    }

    // 4 x 35 year-olds, 3D, 90 minute duration, Thursday, balcony seating == $56.00
    func test6() {
        purchase = MovieTicketPurchase(duration: 90, .thursday, inBalcony: true, is3D: true)
        buy(4, for: 35, false)
        XCTAssertEqual(purchase.finish(), "56.00")
    }
    
    // 4 x 35 year-olds, 2D, 240 minute duration, Monday, normal seating == $50.00
    func test7() {
        purchase = MovieTicketPurchase(duration: 240, .monday)
        buy(4, for: 35, false)
        XCTAssertEqual(purchase.finish(), "50.00")
    }

    // 4 x 35 year-olds, 3D, 90 minute duration, Saturday, balcony seating == $70.00
    func test8() {
        purchase = MovieTicketPurchase(duration: 90, .saturday, inBalcony: true, is3D: true)
        buy(4, for: 35, false)
        XCTAssertEqual(purchase.finish(), "70.00")
    }
    
    // 4 x 9 year-olds, 2D, 90 minute duration, Monday, normal seating == $22.00
    func test9() {
        purchase = MovieTicketPurchase(duration: 90, .monday)
        buy(4, for: 9, false)
        
        XCTAssertEqual(purchase.finish(), "22.00")
    }
    
    // 4 x 67 year-olds, 2D, 90 minute duration, Monday, normal seating == $24.00
    func test10() {
        purchase = MovieTicketPurchase(duration: 90, .monday)
        buy(4, for: 67, false)
        XCTAssertEqual(purchase.finish(), "24.00")
    }

    // 4 x 14 year-old students, 2D, 90 minute duration, Monday, normal seating == $32.00
    func test11() {
        purchase = MovieTicketPurchase(duration: 90, .monday)
        buy(4, for: 14, true)
        XCTAssertEqual(purchase.finish(), "32.00")
    }

    // 1 x each type, 2D, 90 minute duration, Monday, normal seating == $30.50
    func test12() {
        purchase = MovieTicketPurchase(duration: 90, .monday)
        buy(1, for: 35, false)
        buy(1, for: 14, true)
        buy(1, for: 9, false)
        buy(1, for: 67, false)
        XCTAssertEqual(purchase.finish(), "30.50")
    }
    
    // 21 x 9 year-olds, 2D, 90 minute duration, Monday, normal seating == $115.50
    func test13() {
        purchase = MovieTicketPurchase(duration: 90, .monday)
        buy(21, for: 9, false)
        XCTAssertEqual(purchase.finish(), "115.50")
    }

    // 21 x 35 year-old, 2D, 90 minute duration, Thursday, normal seating == $126.00
    func test14() {
        purchase = MovieTicketPurchase(duration: 90, .thursday)
        buy(21, for: 35, false)
        XCTAssertEqual(purchase.finish(), "126.00")
    }

    // 10 x 14 year-old students + 11 x 9 year-olds, 2D, 90 minute duration, Monday, normal seating == $120.50
    func test15() {
        purchase = MovieTicketPurchase(duration: 90, .monday)
        buy(10, for: 14, true)
        buy(11, for: 9, false)
        XCTAssertEqual(purchase.finish(), "120.50")
    }

    // 7 x each type, 3D, 240 minute duration, Thursday, balcony seating == $346.50
    func test16() {
        purchase = MovieTicketPurchase(duration: 240, .thursday, inBalcony: true, is3D: true)
        buy(7, for: 35, false)
        buy(7, for: 14, true)
        buy(7, for: 9, false)
        buy(7, for: 67, false)
        XCTAssertEqual(purchase.finish(), "346.50")
    }



//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
