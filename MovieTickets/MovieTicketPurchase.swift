//
//  Concession.swift
//  MovieTickets
//
//  Created by Dwayne Langley on 5/1/18.
//  Copyright © 2018 Dwayne Langley. All rights reserved.
//

import Foundation

struct MovieTicketPurchase {
    
    // Mirrored the properties in the example, with the exception of the tickets collection.
    var movieDuration : Int, weekDay: Day?, isBalconySeat: Bool, is3D: Bool, tickets : [String : Int]
    
    // Custom init with default values to minimize parameter entries on instantiation.
    init(duration : Int = 0, _ day: Day? = nil, inBalcony: Bool = false, is3D: Bool = false, tickets : [String : Int] = [String : Int]()) {
        self.movieDuration = duration
        self.weekDay = day
        self.isBalconySeat = inBalcony
        self.is3D = is3D
        self.tickets = tickets
    }
    
    // I changed my mind from an Array for spacial complexity 
    mutating func addTicket(for age: Int, isStudent: Bool) {
        switch age {
        case 0..<13: incrementCount(of: Types.child)
        case 65...Int.max: incrementCount(of: Types.senior)
        default:
            if isStudent {
                incrementCount(of: Types.student)
            } else {
                incrementCount(of: Types.adult)
            }
        }
    }
    
    func finish() -> String {
        var amount = 0.0
        // Handle 0 tickets
        guard tickets.total > 0 else { return String(format: "%.2f", amount) }
        
        // Apply default base rates.
        let baseRate = Double(tickets.total) * 11
        
        // Handle bundle cases
        guard tickets.total < 20 else {
            let adults = Double(tickets.total - (tickets[Types.child] ?? 0)) * 6
            let children = Double(tickets[Types.child] ?? 0) * 5.50
            amount = adults + children + fees
            return String(format: "%.2f", amount)
        }
        
        // Tally the total from calculated deductions and fees.
        amount = baseRate - deductions + fees
        
        return String(format: "%.2f", amount)// NSString(format: "%.2f", amount) as String
    }
}

extension MovieTicketPurchase { // MARK:- Helpers
    struct Types {
        static let adult = "adult", child = "child", senior = "senior", student = "student"
    }
    
    // calculate individual deductions
    fileprivate var deductions : Double {
        var total: Double = 0
        if let studentDiscounts = tickets[Types.student] { total += Double(studentDiscounts) * 3 }
        if let seniorDiscounts = tickets[Types.senior] { total += Double(seniorDiscounts) * 5 }
        if let childrenDiscount = tickets[Types.child] { total += Double(childrenDiscount) * 5.5 }
        if weekDay == .thursday { total += Double(tickets.total) * 2 }
        return total
    }
    
    // calculate individual fees
    fileprivate var fees : Double {
        var total = 0.00
        if is3D { total += Double(tickets.total) * 3 }
        if isBalconySeat { total += Double(tickets.total) * 2 }
        if weekDay == .saturday || weekDay == .sunday { total += Double(tickets.total) * 1.50 }
        if movieDuration > 120 { total += Double(tickets.total) * 1.50 }
        return total
    }
    
    // hash-burger helper
    fileprivate mutating func incrementCount(of type: String) {
        if let amount = tickets[type] {
            tickets[type] = amount + 1
        } else {
            tickets[type] = 1
        }
    }
}

enum Day {
    case sunday, monday, tuesday, wednesday, thursday, friday, saturday
}

extension Dictionary where Key: ExpressibleByStringLiteral, Value: ExpressibleByIntegerLiteral {
    var total : Int {
        var sum = 0
        for value in self.values {
            sum += value as! Int
        }
        return sum
    }
}
